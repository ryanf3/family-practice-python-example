# import numpy as np
# import scipy as sp

class human:

    #Constructor Class
    def __init__(self,firstName,middleName,lastName,age,favoriteColor, father, mother):
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.age = age
        self.favoriteColor = favoriteColor
        self.father = father
        self.mother = mother
        self.fullName = str(firstName) + " " + str(middleName) + " " + str(lastName)

    def birthday(self):
        self.age+=1

class family:

    def __init__(self, husband, familyName, wife):
        self.husband = husband
        self.familyName = familyName
        self.wife = wife
        self.children = []
        self.grandchildren = []

    def baby(self, firstName, middleName, favoriteColor, isGrandChild):
        newChild = human(firstName, middleName, self.familyName, 0, favoriteColor, self.husband, self.wife)
        if(isGrandChild):
            self.grandchildren.append(newChild)
        else:
            self.children.append(newChild)

ryan = human("Ryan", "Edward", "Flynn", 19, "Orange",None,None)
shan = human("Shaniquah","Shan","S",20, "Pink", None, None)
print(ryan.fullName)
Flynn = family("Ryan", "Flynn", "Shaniquah")
Flynn.baby("Damian","Parks","Red", 0)
print(Flynn.children[0].fullName)
Zech = human("Zech", "Eric", "Flynn", 27, "Blue", ryan, shan)
Barb = human("Barb", "bbb", "Quavo", 24, "Gold", None, None)
Quavo = family("Zech","Quavo","Barb")
Flynn.baby("Huncho","Travis","Black", 1)
print(Flynn.grandchildren[0].fullName)
